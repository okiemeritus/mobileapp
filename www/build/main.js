webpackJsonp([0],{

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 155:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 155;

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shop_shop__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__leaderboard_leaderboard__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__user_user__["b" /* UserPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__shop_shop__["a" /* ShopPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__leaderboard_leaderboard__["a" /* LeaderboardPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="person"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Shop" tabIcon="cart"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="LeaderBoard" tabIcon="contacts"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Product */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShopPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_service_rest_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Product = /** @class */ (function () {
    function Product(item, stock, coins) {
        this.item = item;
        this.stock = stock;
        this.coins = coins;
    }
    return Product;
}());

var ShopPage = /** @class */ (function () {
    function ShopPage(navCtrl, loadingCtrl, httpClient, storage, alertCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.httpClient = httpClient;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.BackendUrl = __WEBPACK_IMPORTED_MODULE_4__user_user__["a" /* ConfigUrls */].shopBackendUrl;
    }
    ShopPage.prototype.ionViewWillEnter = function () {
        this.refreshProducts();
    };
    ShopPage.prototype.refreshProducts = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.httpClient.getProducts(this.BackendUrl).subscribe(function (response) {
            if (response.status == 200) {
                _this.productList = [];
                var productSON = JSON.stringify(response.body);
                var parsedProductdata = JSON.parse(productSON);
                for (var i = 0; i < parsedProductdata.length; i++) {
                    var user = new Product(parsedProductdata[i]['item'], parsedProductdata[i]['stock'] + " left", parsedProductdata[i]['coins'] + " fitcoins");
                    _this.productList.push(user);
                }
                loading.dismiss();
            }
            else {
                loading.dismiss();
                _this.showError();
            }
        });
    };
    ShopPage.prototype.buyProduct = function (Product) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Ordering...'
        });
        loading.present();
        this.storage.get('userName').then(function (userId) {
            _this.httpClient.buyProduct(_this.BackendUrl, Product.item, userId).subscribe(function (result) {
                loading.dismiss();
                if (result['status'] == 'Insufficient fitcoin balance') {
                    _this.showFailureAlert();
                }
                else {
                    _this.showSuccessAlert();
                }
            });
        });
    };
    ShopPage.prototype.showSuccessAlert = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Order successfully placed',
            buttons: [{
                    text: 'Ok',
                    handler: function () {
                        _this.refreshProducts();
                    }
                }]
        });
        alert.present();
    };
    ShopPage.prototype.showFailureAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Failure',
            subTitle: 'Insufficient fitcoin balance',
            buttons: ['Ok']
        });
        alert.present();
    };
    ShopPage.prototype.showError = function () {
        var alert = this.alertCtrl.create({
            title: 'Failure',
            subTitle: 'Connection Error, Kindly try after sometime.',
            buttons: ['Ok']
        });
        alert.present();
    };
    ShopPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-shop',template:/*ion-inline-start:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/shop/shop.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Shop\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page4">\n  <div id="shop-container2">\n    <ion-list id="shop-list3">\n      <ion-item color="none" id="shop-list-item10" *ngFor="let product of productList">\n        <h1>{{product.item}}</h1>\n        <p>{{product.stock}}</p>\n        <h3 item-right>{{product.coins}}</h3>\n        <button ion-button clear (click)="buyProduct(product)" item-end>Buy</button>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/shop/shop.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_service_rest_service__["a" /* RestServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ShopPage);
    return ShopPage;
}());

//# sourceMappingURL=shop.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export User */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaderboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_service_rest_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var User = /** @class */ (function () {
    function User(name, image, steps) {
        this.name = name;
        this.image = image;
        this.steps = steps;
    }
    return User;
}());

var LeaderboardPage = /** @class */ (function () {
    function LeaderboardPage(navCtrl, loadingCtrl, httpClient, sanitizer, alertCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.alertCtrl = alertCtrl;
        this.BackendUrl = __WEBPACK_IMPORTED_MODULE_4__user_user__["a" /* ConfigUrls */].leaderboardBackendUrl;
    }
    LeaderboardPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.httpClient.getLeaderBoard(this.BackendUrl).subscribe(function (response) {
            if (response.status == 200) {
                _this.userList = [];
                var userJSON = JSON.stringify(response.body);
                var parsedUserdata = JSON.parse(userJSON);
                for (var i = 0; i < parsedUserdata.length; i++) {
                    var user = new User((i + 1).toString() + ". " + parsedUserdata[i]['userid'], "data:image/png;base64, " + parsedUserdata[i]['image'], parsedUserdata[i]['steps'] + ' steps');
                    _this.userList.push(user);
                }
                loading.dismiss();
            }
            else {
                loading.dismiss();
                _this.showError();
            }
        });
    };
    LeaderboardPage.prototype.showError = function () {
        var alert = this.alertCtrl.create({
            title: 'Failure',
            subTitle: 'Connection Error, Kindly try after sometime.',
            buttons: ['Ok']
        });
        alert.present();
    };
    LeaderboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-leaderboard',template:/*ion-inline-start:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/leaderboard/leaderboard.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Leaderboard\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page6">\n  <div id="leaderboard-container4">\n    <ion-list id="leaderboard-list4">\n      <ion-item color="none" id="leaderboard-list-item14" *ngFor="let user of userList">\n        <ion-avatar item-left>\n          <img [src]="sanitizer.bypassSecurityTrustUrl(user.image)" />\n        </ion-avatar>\n        <h1>{{user.name}}</h1>\n        <h3 item-right>{{user.steps}}</h3>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/leaderboard/leaderboard.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_service_rest_service__["a" /* RestServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LeaderboardPage);
    return LeaderboardPage;
}());

//# sourceMappingURL=leaderboard.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(225);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_shop_shop__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_leaderboard_leaderboard__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common_http__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_rest_service_rest_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_storage__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_shop_shop__["a" /* ShopPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_user_user__["b" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_leaderboard_leaderboard__["a" /* LeaderboardPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_12__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_10__angular_common_http__["b" /* HttpClientModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_shop_shop__["a" /* ShopPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_user_user__["b" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_leaderboard_leaderboard__["a" /* LeaderboardPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_11__providers_rest_service_rest_service__["a" /* RestServiceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestServiceProvider = /** @class */ (function () {
    function RestServiceProvider(http) {
        this.http = http;
        // 'Hello RestServiceProvider Provider'
    }
    RestServiceProvider.prototype.registerUser = function (BackendUrl, avatar) {
        var headers = {
            'content-type': 'application/json'
        };
        return this.http.post(BackendUrl + '/users', avatar, { headers: headers });
    };
    RestServiceProvider.prototype.getUsers = function (BackendUrl) {
        return this.http.get(BackendUrl + '/users');
    };
    RestServiceProvider.prototype.getLeaderBoard = function (BackendUrl) {
        return this.http.get(BackendUrl + '/leaderboard', {
            observe: 'response'
        });
    };
    RestServiceProvider.prototype.getProducts = function (BackendUrl) {
        return this.http.get(BackendUrl + '/shop/products', {
            observe: 'response'
        });
    };
    RestServiceProvider.prototype.buyProduct = function (BackendUrl, product, userName) {
        var headers = {
            'content-type': 'application/json'
        };
        var data = {
            'name': userName
        };
        return this.http.post(BackendUrl + '/shop/order/' + product, data, { headers: headers });
    };
    RestServiceProvider.prototype.addSteps = function (BackendUrl, steps, userID) {
        var headers = {
            'content-type': 'application/json'
        };
        var data = {
            'steps': steps
        };
        return this.http.put(BackendUrl + '/users/' + userID, data, { headers: headers });
    };
    RestServiceProvider.prototype.getUserInfo = function (BackendUrl, userID) {
        return this.http.get(BackendUrl + '/users/' + userID, {
            observe: 'response'
        });
    };
    RestServiceProvider.prototype.generateAvatar = function (BackendUrl) {
        return this.http.get(BackendUrl + '/users/generate', {
            observe: 'response'
        });
    };
    RestServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestServiceProvider);
    return RestServiceProvider;
}());

//# sourceMappingURL=rest-service.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigUrls; });
/* unused harmony export UserAvatar */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_service_rest_service__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ConfigUrls = /** @class */ (function () {
    function ConfigUrls() {
    }
    // Add your microservice backend urls
    // Note: Ideally, all of them will point to the same URL if the backend microservices are exposed 
    // via Ingress. 
    // For convenience of running the sample on Lite IBM Kubernetes clusters, we have
    // separated the configuration to include URLs from all microservices here
    ConfigUrls.userBackendUrl = "http://35.240.178.11:8080";
    ConfigUrls.shopBackendUrl = "http://34.87.37.6:8082";
    ConfigUrls.leaderboardBackendUrl = "http://35.247.140.80:8081";
    return ConfigUrls;
}());

var UserAvatar = /** @class */ (function () {
    function UserAvatar(response) {
        this.image = response['image'];
        this.name = response['name'];
    }
    return UserAvatar;
}());

var UserPage = /** @class */ (function () {
    function UserPage(navCtrl, httpClient, sanitizer, alertCtrl, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.BackendUrl = ConfigUrls.userBackendUrl;
    }
    UserPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('userName').then(function (userId) {
            if (userId == null) {
                // Fresh User
                _this.registerUser();
            }
            else {
                // Returning User
                _this.updateUserDetails(userId, true);
            }
        });
    };
    UserPage.prototype.addSteps = function () {
        var _this = this;
        this.httpClient.addSteps(this.BackendUrl, '10', this.userName).subscribe(function (response) {
            var alert = _this.alertCtrl.create({
                title: 'Success',
                subTitle: 'We have added 10 steps to your account',
                buttons: ['Cool']
            });
            alert.present();
            _this.updateUserDetails(_this.userName, false);
        });
    };
    UserPage.prototype.registerUser = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.httpClient.generateAvatar(this.BackendUrl).subscribe(function (response) {
            if (response.status == 200) {
                var useravatar = new UserAvatar(response.body);
                _this.httpClient.registerUser(_this.BackendUrl, response.body).subscribe(function (Response) {
                    loading.dismiss();
                    _this.userName = Response['name'];
                    _this.storage.set('userName', _this.userName);
                    _this.userFitcoins = Response['fitcoins'] + " fitcoins";
                    _this.displayImage = "data:image/png;base64, " + useravatar.image;
                    _this.presentAlert();
                });
            }
            else {
                loading.present();
                _this.showError();
            }
        });
    };
    UserPage.prototype.updateUserDetails = function (userName, showIndicator) {
        var _this = this;
        var loading;
        if (showIndicator) {
            loading = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
        }
        this.httpClient.getUserInfo(this.BackendUrl, userName).subscribe(function (response) {
            if (response.status == 200) {
                _this.userFitcoins = response.body[0]['fitcoins'] + " fitcoins";
                _this.userName = response.body[0]['userid'];
                _this.displayImage = "data:image/png;base64, " + response.body[0].image;
                if (showIndicator) {
                    loading.dismiss();
                }
            }
            else {
                if (showIndicator) {
                    loading.dismiss();
                }
                _this.showError();
            }
        });
    };
    UserPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Hi, ' + this.userName,
            subTitle: 'You were enrolled and given this random name and avatar.',
            buttons: ['Cool']
        });
        alert.present();
    };
    UserPage.prototype.showError = function () {
        var alert = this.alertCtrl.create({
            title: 'Failure',
            subTitle: 'Connection Error, Kindly try after sometime.',
            buttons: ['Ok']
        });
        alert.present();
    };
    UserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user',template:/*ion-inline-start:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/user/user.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      User\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n  <div id="user-container3" *ngIf="displayImage">\n    <img [src]="sanitizer.bypassSecurityTrustUrl(displayImage)"\n      style="display:block;width:150px;height:auto;margin-left:auto;margin-right:auto;" />\n  </div>\n  <h3 id="user-heading3" style="color:#000000;text-align:center;">\n    {{userName}}\n  </h3>\n  <h4 id="user-heading4" style="color:#000000;text-align:center;">\n    {{userFitcoins}}\n  </h4>\n  <div id="user-container3" *ngIf="displayImage">\n    <button id="recharge-button" ion-button (click)="addSteps()" color="stable" block>\n      Add Steps\n    </button>\n  </div>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/emeritus123/mobile-app-and-microservices-with-ibm-cloud/fitlead-ionic-app/src/pages/user/user.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_rest_service_rest_service__["a" /* RestServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], UserPage);
    return UserPage;
}());

//# sourceMappingURL=user.js.map

/***/ })

},[204]);
//# sourceMappingURL=main.js.map